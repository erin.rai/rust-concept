
use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("Guess the number!"); // println prints the text in new line

    let secret_number = rand::thread_rng().gen_range(1..101); // generates the random number

    loop {
        println!("Please input your guess.");

        let mut guess = String::new(); // initialized the mutable variable of type string,

        // Handles the input from the user
        // & indicates that this arguement is a reference, gives you a way to let multiple part of the code to access one piece of data
        // without needing to copy that data into memoery multiple times.
        // handles potential failures
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        // println!("x = {} and y = {}", x, y); example of {} holding the values
        println!("You guessed:{}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too Small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
